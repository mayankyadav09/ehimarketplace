package com.ehi.marketplace.core.beans;

public class AzureUser {

    private String displayName;
    private String mail;
    private String id;

    public String getDisplayName() {
        return displayName;
    }

    public String getMail() {
        return mail;
    }

    public String getId() {
        return id;
    }
}
