package com.ehi.marketplace.core.beans.api;

import org.apache.http.NameValuePair;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.util.List;

public class RequestBody {
    private StringEntity jsonEntity;
    private MultipartEntityBuilder multiPartEntity;
    private List<NameValuePair> bodyParams;

    public RequestBody(StringEntity jsonEntity) {
        this.jsonEntity = jsonEntity;
    }

    public RequestBody(List<NameValuePair> bodyParams) {
        this.bodyParams = bodyParams;
    }

    public List<NameValuePair> getBodyParams() {
        return bodyParams;
    }

    public StringEntity getJsonEntity() {
        return jsonEntity;
    }

    public MultipartEntityBuilder getMultiPartEntity() {
        return multiPartEntity;
    }
}
