package com.ehi.marketplace.core.beans.api;

import org.apache.http.Header;
import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

public class RequestObject {
    private RequestBody requestBody;
    private String requestUrl;
    private List<Header> headerList;
    private List<NameValuePair> parametersList;

    public RequestObject(String requestUrl, List<Header> headerList) {
        this.requestUrl = requestUrl;
        this.headerList = headerList;
    }

    public RequestObject(RequestBody requestBody, String requestUrl, List<Header> headerList) {
        this.requestBody = requestBody;
        this.requestUrl = requestUrl;
        this.headerList = headerList;
    }

    public RequestObject(RequestBody requestBody, String requestUrl, List<Header> headerList, List<NameValuePair> parametersList) {
        this(requestBody, requestUrl, headerList);
        this.parametersList = parametersList;
    }

    public RequestBody getRequestBody() {
        return requestBody;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public List<Header> getHeaderList() {
        return headerList != null ? new ArrayList<>(headerList) : new ArrayList<>();
    }

    public List<NameValuePair> getParametersList() {
        return parametersList != null ? new ArrayList<>(parametersList) : new ArrayList<>();
    }
}
