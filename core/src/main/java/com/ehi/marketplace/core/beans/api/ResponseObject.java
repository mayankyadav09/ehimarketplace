package com.ehi.marketplace.core.beans.api;

public class ResponseObject {
    private String httpJsonResponse;

    public String getHttpJsonResponse() {
        return httpJsonResponse;
    }

    public void setHttpJsonResponse(String httpJsonResponse) {
        this.httpJsonResponse = httpJsonResponse;
    }
}
