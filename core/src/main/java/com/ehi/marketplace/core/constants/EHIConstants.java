package com.ehi.marketplace.core.constants;

/**
 * This class provides global constants.
 * <p>
 * When relevant, constant names are always prefixed with a type qualifier (NT for a node type, PN for a property, RT
 * for a resource type...)
 */
public final class EHIConstants {

	private EHIConstants() {
	}

	public static final String PAGE_PATH = "/content/ehimarketplace/language-masters/en";
	public static final String TEMPLATE_PATH = "/conf/ehimarketplace/settings/wcm/templates/api-spec-page";
	public static final String API_SPEC_ID = "apiSpecId";
	public static final String API_SPEC_PATH = "/content/ehimarketplace";
	public static final String EHI_WRITE_DATA_SUB_SERVICE = "ehi-write-data";
	public static final String API_VERSION_JCR = "version";
	public static final String ERROR_RECORD_FILE_JCR_PATH = "/content/dam/ehimarketplace/Records.txt/jcr:content/renditions/original/jcr:content";
	public static final String DAM_ASSET = "dam:Asset";
	public static final String ASSET_JCR_CONTENT = "/jcr:content/renditions/original/jcr:content";
	public static final String JCR_CONTENT = "/jcr:content";
	public static final String API_SPEC_UPDATED = "API Spec Created!";
	public static final String API_SPEC_NOT_UPDATED = "Api spec not updated";
	public static final String API_SPEC_YAML_FILENAME = "apispec.yaml";
	public static final String DATA = "data";
	public static final String REQUEST_PATH = "requestPath";
	public static final String VERSION = "version";
	public static final String ENDPOINTS = "-endpoints";
	public static final String ERROR = "error-";
	public static final String API_SPEC_DATA = "apiSpecData";
	public static final String JSON_FILE_PATH = "/content/dam/ehimarketplace/error.json";
	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String AUTHOR = "author";
	public static final String TITLE = "title";
	public static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
	public static final String APPLICATION_JSON = "application/json";

	public static final String SLASH = "/";

	public static final String EXTERNAL_USER_GROUP = "external-users";

	public static final String HTML_EXTENSION = ".html";

	public static final String KEYS_URL = "https://tczorg.b2clogin.com/tczorg.onmicrosoft.com/b2c_1_ehi_sign_in/discovery/v2.0/keys";
	public static final String AZURE_GROUPS = "azure_groups";
	public static final String AUTHORIZATION_CODE = "authorization_code";
	public static final String CODE = "code";
	public static final String STATE = "state";
	public static final String AUTH_TOKEN = "authToken";
	public static final String ACCESS_TOKEN = "access_token";
	public static final String REFRESH_TOKEN = "refresh_token";
	public static final String SCOPE = "scope";
	public static final String CLIENT_ID = "client_id";
	public static final String CLIENT_SECRET = "client_secret";
	public static final String GRANT_TYPE = "grant_type";
	public static final String CODE_VERIFIER = "code_verifier";
	public static final String CODE_CHALLENGE = "code_challenge";
	public static final String REDIRECT_URI = "redirect_uri";
	public static final String RESPONSE_TYPE = "response_type";
	public static final String RESPONSE_MODE = "response_mode";
	public static final String QUERY = "query";

	public static final String MESSAGE = "message";
	public static final String STATUS = "status";
	public static final String USER_LOGGED_OUT = "User Successfully Logged Out";

}
