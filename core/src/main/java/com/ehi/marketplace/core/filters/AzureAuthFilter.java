/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.ehi.marketplace.core.filters;

import com.adobe.granite.crypto.CryptoException;
import com.adobe.granite.crypto.CryptoSupport;
import com.auth0.jwk.JwkException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ehi.marketplace.core.beans.AzureGroup;
import com.ehi.marketplace.core.services.GraphAPIService;
import com.ehi.marketplace.core.services.configurations.GraphAPIServiceConfig;
import com.ehi.marketplace.core.utils.EHIUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.EngineConstants;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceRanking;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static com.ehi.marketplace.core.constants.EHIConstants.ACCESS_TOKEN;
import static com.ehi.marketplace.core.constants.EHIConstants.AUTH_TOKEN;
import static com.ehi.marketplace.core.constants.EHIConstants.AZURE_GROUPS;
import static com.ehi.marketplace.core.constants.EHIConstants.CLIENT_ID;
import static com.ehi.marketplace.core.constants.EHIConstants.CODE;
import static com.ehi.marketplace.core.constants.EHIConstants.CODE_CHALLENGE;
import static com.ehi.marketplace.core.constants.EHIConstants.EHI_WRITE_DATA_SUB_SERVICE;
import static com.ehi.marketplace.core.constants.EHIConstants.HTML_EXTENSION;
import static com.ehi.marketplace.core.constants.EHIConstants.QUERY;
import static com.ehi.marketplace.core.constants.EHIConstants.REDIRECT_URI;
import static com.ehi.marketplace.core.constants.EHIConstants.REFRESH_TOKEN;
import static com.ehi.marketplace.core.constants.EHIConstants.RESPONSE_MODE;
import static com.ehi.marketplace.core.constants.EHIConstants.RESPONSE_TYPE;
import static com.ehi.marketplace.core.constants.EHIConstants.SCOPE;
import static com.ehi.marketplace.core.constants.EHIConstants.STATE;
import static com.ehi.marketplace.core.utils.EHIUtils.getListOfGroupsForPage;
import static com.ehi.marketplace.core.utils.EHIUtils.isTokenExpired;
import static com.ehi.marketplace.core.utils.EHIUtils.verifyUserGroup;

/**
 * Simple servlet filter component that logs incoming requests.
 */
@Component(service = Filter.class, property = {
        EngineConstants.SLING_FILTER_SCOPE + "=" + EngineConstants.FILTER_SCOPE_REQUEST,
        EngineConstants.SLING_FILTER_PATTERN + "=" + "/content/ehimarketplace/.*"})
@ServiceDescription("EHI Auth Filter")
@ServiceRanking(-1500)
@ServiceVendor("EHI Marketplace")
public class AzureAuthFilter implements Filter {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String PUBLISH = "publish";
    private static final String REDIRECT_URL = "/content/ehimarketplace/language-masters/en/errors/403.html";
    private static final String ERROR_PAGE_URL = "/content/ehimarketplace/language-masters/en/errors/500.html";

    @Activate
    private GraphAPIServiceConfig config;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private CryptoSupport cryptoSupport;

    @Reference
    private SlingSettingsService slingSettingsService;

    @Reference
    private GraphAPIService graphAPIService;

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {
        final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
        final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;
        if (slingSettingsService.getRunModes().contains(PUBLISH)) {
            String requestPath = slingRequest.getRequestPathInfo().getResourcePath();
            String authCookieVal = EHIUtils.getCookieValue(slingRequest, AUTH_TOKEN);
            try (ResourceResolver resolver = EHIUtils.getResourceResolver(resourceResolverFactory, EHI_WRITE_DATA_SUB_SERVICE)) {
                URIBuilder uriBuilder = new
                        URIBuilder(config.ehiAuthorizationURI())
                        .addParameter(CLIENT_ID, config.ehiAuthClientId())
                        .addParameter(RESPONSE_TYPE, CODE)
                        .addParameter(REDIRECT_URI, config.ehiAuthRedirectURI())
                        .addParameter(RESPONSE_MODE, QUERY)
                        .addParameter(SCOPE, config.authScope())
                        .addParameter(STATE, requestPath)
                        .addParameter(CODE_CHALLENGE, config.ehiAuthCodeVerifier());
                if (StringUtils.isBlank(authCookieVal)) {
                    logger.info("doFilter(...) [Info] Cookie Doesn't exist, Redirecting to Azure AD B2C");
                    String redirectURL = uriBuilder.build().toString();
                    slingResponse.sendRedirect(redirectURL);
                } else {
                    logger.info("doFilter(...) [Info] Cookie exists, Validating the request");
                    Resource requestedResource = resolver.resolve(requestPath);
                    processRequest(requestedResource, resolver, filterChain, authCookieVal,
                            request, response, slingResponse);
                }
            } catch (URISyntaxException | LoginException e) {
                logger.error("doFilter(...) [Error] URISyntaxException | LoginException : ", e);
                slingResponse.sendRedirect(ERROR_PAGE_URL);
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }

    /**
     * Method to process/handle the Request, if authCookie is present
     *
     * @param requestedResource
     * @param resolver
     * @param filterChain
     * @param authCookieVal
     * @param request
     * @param response
     * @param slingResponse
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(final Resource requestedResource, final ResourceResolver resolver, final FilterChain filterChain,
                                final String authCookieVal, final ServletRequest request, final ServletResponse response,
                                final SlingHttpServletResponse slingResponse) throws IOException {

        try {
            if (getListOfGroupsForPage(requestedResource.getPath(), resolver).isEmpty()) {
                logger.info("doFilter(...) [Info] Group not present/matched, hence allowing request!");
                filterChain.doFilter(request, response);
            } else {
                if (isValidRequest(requestedResource.getPath(), authCookieVal, slingResponse)) {
                    logger.info("doFilter(...) [Info] Hence, Request Validated");
                    filterChain.doFilter(request, response);
                } else {
                    logger.info("doFilter(...) [Info] Request Invalidated!");
                    logger.info("doFilter(...) [Info] Redirect to: {}", REDIRECT_URL);
                    slingResponse.sendRedirect(REDIRECT_URL);
                }
            }
        } catch (ServletException e) {
            slingResponse.sendRedirect(ERROR_PAGE_URL);
            logger.error("processRequest(...) [Error] ServletException : ", e);
        }
    }

    /**
     * Method to check whether the request is valid or not basis on the Auth Cookie Value
     *
     * @param requestPath   Request Path
     * @param authCookieVal Auth Cookie Value
     * @return {boolean}
     */
    private boolean isValidRequest(final String requestPath, final String authCookieVal, final SlingHttpServletResponse slingResponse)
            throws IOException {
        boolean isValid = false;
        Gson gson = new Gson();
        try (ResourceResolver resolver = EHIUtils.getResourceResolver(resourceResolverFactory, EHI_WRITE_DATA_SUB_SERVICE)) {
            Session session = resolver.adaptTo(Session.class);
            if (session != null) {
                String decryptedCookieVal = cryptoSupport.unprotect(authCookieVal);
                JsonObject jsonObject = new JsonParser().parse(decryptedCookieVal).getAsJsonObject();
                if (jsonObject.has(ACCESS_TOKEN)) {
                    String accessToken = jsonObject.get(ACCESS_TOKEN).getAsString();
                    List<AzureGroup> azureGroupList = gson.fromJson(jsonObject.getAsJsonArray(AZURE_GROUPS),
                            new TypeToken<List<AzureGroup>>() {
                            }.getType());
                    DecodedJWT jwt = graphAPIService.verifyToken(accessToken);
                    logger.debug("isValidRequest(...) [Debug] Valid JWT Token");
                    isValid = isValidResource(jwt, resolver, requestPath, azureGroupList, jsonObject, slingResponse);
                }
            } else {
                logger.error("isValidRequest(...) [Error] Session Null");
                slingResponse.sendRedirect(ERROR_PAGE_URL);
            }
        } catch (CryptoException | JwkException | IOException | LoginException e) {
            logger.error("isValidRequest(...) [Error] CryptoException | JwkException | IOException | LoginException: ", e);
            slingResponse.sendRedirect(ERROR_PAGE_URL);
        }
        return isValid;
    }

    /**
     * Method to check the requested resource is valid or not!
     *
     * @param jwt         Decoded JWT Token
     * @param resolver    ResourceResolver
     * @param requestPath Request Path
     * @return {boolean}
     */
    private boolean isValidResource(DecodedJWT jwt, final ResourceResolver resolver, final String requestPath,
                                    final List<AzureGroup> azureGroupList, final JsonObject jsonObject,
                                    final SlingHttpServletResponse slingResponse) throws IOException {
        boolean isValid = false;
        try {
            if (isTokenExpired(jwt)) {
                logger.info("isValidResource(...) [Info] Token Expired!");
                JsonObject accessTokenJson = graphAPIService
                        .acquireAccessTokenFromRefreshToken(jsonObject.get(REFRESH_TOKEN).getAsString());
                logger.debug("AccessTokenJSON via Refresh Token: {}", accessTokenJson.get(ACCESS_TOKEN).getAsString());
                JsonObject authObject = new JsonObject();
                JsonElement jsonElement = new Gson().toJsonTree(azureGroupList);
                authObject.addProperty(ACCESS_TOKEN, accessTokenJson.get(ACCESS_TOKEN).getAsString());
                authObject.addProperty(REFRESH_TOKEN, accessTokenJson.get(REFRESH_TOKEN).getAsString());
                authObject.add(AZURE_GROUPS, jsonElement);
                EHIUtils.setCookie(slingResponse, cryptoSupport.protect(authObject.toString()), AUTH_TOKEN, null, 3600);
                slingResponse.sendRedirect(requestPath + HTML_EXTENSION);
            } else {
                if (verifyUserGroup(resolver, requestPath, azureGroupList)) {
                    logger.info("isValidResource(...) [Info] User Group Verified");
                    isValid = true;
                } else {
                    logger.info("isValidResource(...) [Info] User Group not verified");
                }
            }
        } catch (CryptoException e) {
            logger.error("isValidResource(...) [Error] CryptoException: ", e);
            slingResponse.sendRedirect(ERROR_PAGE_URL);
        }
        return isValid;
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }
}