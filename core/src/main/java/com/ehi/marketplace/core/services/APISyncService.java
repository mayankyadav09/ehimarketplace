package com.ehi.marketplace.core.services;

import java.util.List;

import javax.jcr.Session;

import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.ResourceResolver;
import org.json.JSONObject;

import com.day.cq.wcm.api.Page;

import io.swagger.v3.oas.models.OpenAPI;

public interface APISyncService {	
	/**
	 * This function is used to fetch API Spec Pages 
	 * @param session
	 * @return
	 */

	List<Page> getAPISpecPages(Session session);

	/**
	 * This function is used to fetch API Spec Page Through API Spec Id
	 * 
	 * @param session
	 * @param apiSpecId
	 * @return
	 */
	List<Page> getAPISpecPageFromId(Session session, String apiSpecId);

	/**
	 * This function will be used to put properties in a apispecpage
	 * @param page
	 * @param resolver
	 * @param openAPI
	 * @param apiSpecData
	 * @throws PersistenceException
	 */
	void putPropertiesInAPISpecPage(Page page, ResourceResolver resolver,OpenAPI openAPI, String apiSpecData) throws PersistenceException;


	/**
	 * 
	 * This function will be used to create any File inside content Node
	 * @param page
	 * @param resolver
	 * @param apiSpecJsonData
	 * @param mimetype
	 * @param filename
	 * @throws PersistenceException
	 */
	void createFileinJcr(Page page, ResourceResolver resolver,String apiSpecJsonData,String mimetype, String filename) throws PersistenceException;

	/**
	 * 
	 * @param errorObj
	 * @param uniqueKey
	 */
	public void storeErrorLogs(JSONObject errorObj, String uniqueKey, String errorMessage);

}
