package com.ehi.marketplace.core.services;

import com.auth0.jwk.JwkException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ehi.marketplace.core.beans.AzureGroup;
import com.ehi.marketplace.core.beans.AzureUser;
import com.google.gson.JsonObject;
import org.osgi.annotation.versioning.ProviderType;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;

@ProviderType
public interface GraphAPIService {


    /**
     * Method to get the list of azure users
     *
     * @param accessToken Access Token
     * @return {List<AzureUsers>}
     */
    List<AzureUser> getAzureUsers(String accessToken) throws URISyntaxException, IOException;

    /**
     * Method to get the list of groups basis on the user id
     *
     * @param userId      UserID
     * @param accessToken Access Token
     * @return {List<AzureGroups>}
     */
    List<AzureGroup> getUserGroups(String userId, String accessToken) throws URISyntaxException, IOException;

    /**
     * Method to acquire the access token via Refresh Token
     *
     * @param refreshToken
     * @return
     */
    JsonObject acquireAccessTokenFromRefreshToken(String refreshToken) throws IOException;

    /**
     * Method to acquire the access token for Auth Code
     *
     * @param authCode Authorization Code
     * @return {JsonObject}
     */
    JsonObject acquireAccessTokenForAuthCode(final String authCode) throws IOException;

    /**
     * Method to return the access token for GraphAPI
     *
     * @return {String}
     */
    String acquireAccessTokenForGraphAPI() throws IOException;

    /**
     * Method to extract the JWT from AccessToken and verify it
     *
     * @param accessToken AccessToken
     * @return {DecodedJWT}
     * @throws MalformedURLException
     * @throws JwkException
     */
    DecodedJWT verifyToken(final String accessToken) throws MalformedURLException, JwkException;
}
