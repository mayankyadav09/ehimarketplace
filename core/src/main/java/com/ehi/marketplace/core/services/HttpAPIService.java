package com.ehi.marketplace.core.services;

import com.ehi.marketplace.core.beans.api.RequestObject;
import org.osgi.annotation.versioning.ProviderType;

import java.io.IOException;
import java.net.URISyntaxException;

@ProviderType
public interface HttpAPIService {

    /**
     * Method to execute the GET Request
     *
     * @param request Request Object contains all the request configurable params
     * @return {@link String}
     */
    String executeGetRequest(final RequestObject request) throws URISyntaxException, IOException;

    /**
     * Method to execute the POST request
     *
     * @param requestObject Request Object contains all the request configurable params
     * @return {@link String}
     */
    String executePostRequest(final RequestObject requestObject) throws IOException;

    /**
     * Method to execute the DELETE request
     *
     * @param request Request Object contains all the request configurable params
     * @return {@link String}
     */
    String executeDeleteRequest(final RequestObject request) throws URISyntaxException, IOException;
}
