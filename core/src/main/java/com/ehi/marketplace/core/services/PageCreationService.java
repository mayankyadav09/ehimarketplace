package com.ehi.marketplace.core.services;

import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.annotation.versioning.ProviderType;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMException;

@ProviderType
public interface PageCreationService {

	/**
	 * Method to create Page
	 *
	 *
	 */
	public Page createPage(String pagePath, String pageName, String templatePath, String jcrName, ResourceResolver resolver) throws IllegalArgumentException, WCMException,javax.jcr.PathNotFoundException;

	/**
	 * Method to Check if Page exists or not, will return the page if it exists if not will return null
	 *
	 *
	 */
	Page checkPageExists(String pagePath, ResourceResolver resolver) ;

	/**
	 * Method to Rollout Pages
	 *
	 *
	 */
	void rolloutPage(Page targetPage, ResourceResolver resolver) throws WCMException;

}
