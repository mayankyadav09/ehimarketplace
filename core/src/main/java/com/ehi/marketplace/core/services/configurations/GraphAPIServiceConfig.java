package com.ehi.marketplace.core.services.configurations;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Graph API Configurations", description = "Configure Graph API")
public @interface GraphAPIServiceConfig {

    @AttributeDefinition(name = "Graph App - Scope")
    String scope() default "https://graph.microsoft.com/.default";

    @AttributeDefinition(name = "Graph App - Client ID")
    String clientId() default "ce2f4ed9-39b6-4368-94fc-2b3098edafdc";

    @AttributeDefinition(name = "Graph App - Client Secret")
    String clientSecret() default "tG-7Q~ddpSdVu1VO~b3OmzgkItlYpOjnv2n5D";

    @AttributeDefinition(name = "Grant Type")
    String grantType() default "client_credentials";

    @AttributeDefinition(name = "Tenant ID")
    String tenantId() default "05f75adb-b7bb-428e-b19a-115c79c7f19c";

    @AttributeDefinition(name = "Authorization Scope")
    String authScope() default "e7a21fba-fadc-484d-8ce5-e08d6ce21e15 offline_access";

    @AttributeDefinition(name = "EHI Auth - Access Token Request URI")
    String ehiAuthAccessTokenRequestURI() default "https://tczorg.b2clogin.com/tczorg.onmicrosoft.com/B2C_1_ehi_sign_in/oauth2/v2.0/token";

    @AttributeDefinition(name = "EHI Auth - Authorization URI")
    String ehiAuthorizationURI() default "https://tczorg.b2clogin.com/tczorg.onmicrosoft.com/B2C_1_ehi_sign_in/oauth2/v2.0/authorize";

    @AttributeDefinition(name = "EHI Auth - Keys URI")
    String ehiAuthKeysURI() default "https://tczorg.b2clogin.com/tczorg.onmicrosoft.com/b2c_1_ehi_sign_in/discovery/v2.0/keys";

    @AttributeDefinition(name = "Auth Code Redirect URI")
    String ehiAuthRedirectURI() default "http://localhost:4503/bin/ehi/auth/code/verifier";

    @AttributeDefinition(name = "EHI Auth - Client ID")
    String ehiAuthClientId() default "e7a21fba-fadc-484d-8ce5-e08d6ce21e15";

    @AttributeDefinition(name = "EHI Auth - Client Secret")
    String ehiAuthClientSecret() default ")UXS3Ou\\5\\1&_uu}HC~}viq0";

    @AttributeDefinition(name = "EHI Auth - Code Verifier")
    String ehiAuthCodeVerifier() default "YTFjNjI1OWYzMzA3MTI4ZDY2Njg5M2RkNmVjNDE5YmEyZGRhOGYyM2IzNjdmZWFhMTQ1ODg3NDcxY2Nl";

}
