package com.ehi.marketplace.core.services.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.jcr.Binary;
import javax.jcr.Session;
import javax.jcr.ValueFactory;

import org.apache.commons.io.IOUtils;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceNotFoundException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ResourceUtil;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.AssetManager;
import com.day.cq.wcm.api.Page;
import com.ehi.marketplace.core.constants.EHIConstants;
import com.ehi.marketplace.core.services.APISyncService;
import com.ehi.marketplace.core.services.JcrSearchService;
import com.ehi.marketplace.core.utils.EHIUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import io.swagger.v3.oas.models.OpenAPI;

@Component(service = APISyncService.class)
public class APISyncServiceImpl implements APISyncService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Reference
	private JcrSearchService jcrSearchService;

	@Reference
	private transient ResourceResolverFactory resourceResolverFactory;

	/**
	 * {@inheritDoc}
	 */
	public List<Page> getAPISpecPages(Session session) {
		Map<String, String> predicates = new HashMap<>();
		predicates.put("1_property", "@" + JcrConstants.JCR_CONTENT + "/" + EHIConstants.API_SPEC_ID);
		predicates.put("1_property.operation", "exists");
		predicates.put("2_property", "@" + JcrConstants.JCR_CONTENT + "/" + EHIConstants.API_SPEC_ID);
		predicates.put("2_property.operation", "like");
		predicates.put("2_property.value", "%_%");
		predicates.put("path", EHIConstants.API_SPEC_PATH);
		predicates.put("p.limit", "-1");
		predicates.put("type", "cq:Page");
		return jcrSearchService.getPages(predicates, session);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Page> getAPISpecPageFromId(Session session, String apiSpecId) {
		Map<String, String> predicates = new HashMap<>();
		predicates.put("1_property", "@" + JcrConstants.JCR_CONTENT + "/" + EHIConstants.API_SPEC_ID);
		predicates.put("1_property.value", apiSpecId);
		predicates.put("path", EHIConstants.API_SPEC_PATH);
		predicates.put("p.limit", "-1");
		predicates.put("type", "cq:Page");
		return jcrSearchService.getPages(predicates, session);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void putPropertiesInAPISpecPage(Page page, ResourceResolver resolver,OpenAPI openAPI, String apiSpecData) throws PersistenceException , NullPointerException{
		Resource pageJcrResource = resolver.getResource(page.getPath() + "/jcr:content");
		if (pageJcrResource != null) {
			ModifiableValueMap pageJcrResourceProperties = pageJcrResource.adaptTo(ModifiableValueMap.class);
			pageJcrResourceProperties.put(EHIConstants.API_VERSION_JCR, openAPI.getInfo().getVersion());
			pageJcrResourceProperties.put(EHIConstants.TITLE, openAPI.getInfo().getTitle());
			pageJcrResourceProperties.put(EHIConstants.API_SPEC_DATA, apiSpecData);
			createFileinJcr(page, resolver, apiSpecData, EHIConstants.APPLICATION_OCTET_STREAM, EHIConstants.API_SPEC_YAML_FILENAME);
			resolver.commit();
			log.debug("APISyncServiceImpl :: putPropertiesInAPISpecPage :: MarketPlaceDetailJob Page Updated");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createFileinJcr(Page page, ResourceResolver resolver,String apiSpecJsonData,String mimetype, String filename) throws PersistenceException, ResourceNotFoundException {
		Resource metadataOptionJson = ResourceUtil.getOrCreateResource(
				resolver,
				page.getPath() + "/" + JcrConstants.JCR_CONTENT + "/" + filename,
				Collections.singletonMap(JcrConstants.JCR_PRIMARYTYPE,(Object) JcrConstants.NT_FILE),
				null, false);
		Resource metadataOptionJsonJcrContent = ResourceUtil.getOrCreateResource(
				resolver,
				metadataOptionJson.getPath() + "/" + JcrConstants.JCR_CONTENT,
				Collections.singletonMap(JcrConstants.JCR_PRIMARYTYPE,(Object) JcrConstants.NT_RESOURCE),
				null, false);


		final ModifiableValueMap metadataOptionJsonProprties = metadataOptionJsonJcrContent.adaptTo(ModifiableValueMap.class);
		if (metadataOptionJsonProprties.get(JcrConstants.JCR_DATA) != null) {
			// Removing the exisiting data
			metadataOptionJsonProprties.remove(JcrConstants.JCR_DATA);
		}

		metadataOptionJsonProprties.put(JcrConstants.JCR_MIMETYPE, mimetype);
		metadataOptionJsonProprties.put(JcrConstants.JCR_ENCODING, "utf-8");
		final ByteArrayInputStream bais = new ByteArrayInputStream(apiSpecJsonData.getBytes(StandardCharsets.UTF_8));
		metadataOptionJsonProprties.put(JcrConstants.JCR_DATA, bais);
		log.debug(String.format("createJsonFileinJcr :: JsonPath :: {} ", metadataOptionJson.getPath()));
		resolver.commit();
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void storeErrorLogs(JSONObject errorObj, String uniqueKey, String errorMessage) {
		try(ResourceResolver resolver = EHIUtils
				.getResourceResolver(resourceResolverFactory, EHIConstants.EHI_WRITE_DATA_SUB_SERVICE);){
			Resource resource = resolver.getResource(EHIConstants.JSON_FILE_PATH);
			if(resource != null) {
				//Code used to update the JSON Object obtained from the error.json file
				Asset asset = resource.adaptTo(Asset.class);
				Resource original = asset.getOriginal();
				InputStream content = original.adaptTo(InputStream.class);
				JsonElement element = new JsonParser().parse(new InputStreamReader(content));
				String jsonStr = element.toString();
				JSONObject jsonObj = new JSONObject(jsonStr);
				errorObj.put(EHIConstants.ERROR_MESSAGE,errorMessage);
				jsonObj.put(uniqueKey, errorObj);

				//Code used to add the inserted JSONObject
				byte[] bytes = jsonObj.toString().getBytes(StandardCharsets.UTF_8);
				ByteArrayInputStream byteInpusStream = new ByteArrayInputStream(bytes);
				ValueFactory factory = Objects.requireNonNull(resolver.adaptTo(Session.class)).getValueFactory();
				InputStream inputStream = new ByteArrayInputStream(IOUtils.toByteArray(byteInpusStream));
				Binary binary = factory.createBinary(inputStream);
				AssetManager assetMgr = resolver.adaptTo(AssetManager.class);
				if (assetMgr != null) {
					assetMgr.createOrUpdateAsset(EHIConstants.JSON_FILE_PATH, binary, EHIConstants.APPLICATION_JSON, true);
				}
			}
		} catch (Exception error) {
			log.debug("Exception occured in storeErrorLogs {}", error);
		}
	}
}
