package com.ehi.marketplace.core.services.impl;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ehi.marketplace.core.beans.AzureGroup;
import com.ehi.marketplace.core.beans.AzureUser;
import com.ehi.marketplace.core.beans.api.RequestBody;
import com.ehi.marketplace.core.beans.api.RequestObject;
import com.ehi.marketplace.core.services.GraphAPIService;
import com.ehi.marketplace.core.services.HttpAPIService;
import com.ehi.marketplace.core.services.configurations.GraphAPIServiceConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.ehi.marketplace.core.constants.EHIConstants.ACCESS_TOKEN;
import static com.ehi.marketplace.core.constants.EHIConstants.AUTHORIZATION_CODE;
import static com.ehi.marketplace.core.constants.EHIConstants.CLIENT_ID;
import static com.ehi.marketplace.core.constants.EHIConstants.CLIENT_SECRET;
import static com.ehi.marketplace.core.constants.EHIConstants.CODE;
import static com.ehi.marketplace.core.constants.EHIConstants.CODE_VERIFIER;
import static com.ehi.marketplace.core.constants.EHIConstants.GRANT_TYPE;
import static com.ehi.marketplace.core.constants.EHIConstants.REDIRECT_URI;
import static com.ehi.marketplace.core.constants.EHIConstants.REFRESH_TOKEN;
import static com.ehi.marketplace.core.constants.EHIConstants.SCOPE;
import static com.ehi.marketplace.core.constants.EHIConstants.SLASH;

@Component(service = GraphAPIService.class, immediate = true)
@Designate(ocd = GraphAPIServiceConfig.class)
public class GraphAPIServiceImpl implements GraphAPIService {

    private static final Logger logger = LoggerFactory.getLogger(GraphAPIServiceImpl.class);

    private static final String MICROSOFT_LOGIN_URL = "https://login.microsoftonline.com";
    private static final String GRAPH_API_BASE_URL = "https://graph.microsoft.com/v1.0";
    private static final String USERS_ENDPOINT = "/users";
    private static final String MEMBERS_ENDPOINT = "/memberOf";
    private static final String TOKEN_ENDPOINT = "/oauth2/v2.0/token";

    private static final String VALUE = "value";
    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer ";

    @Activate
    private GraphAPIServiceConfig config;

    @Reference
    private HttpAPIService httpAPIService;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AzureUser> getAzureUsers(String accessToken) throws URISyntaxException, IOException {
        List<AzureUser> usersList = new ArrayList<>();
        String requestUrl = GRAPH_API_BASE_URL + USERS_ENDPOINT;
        Header authHeader = new BasicHeader(AUTHORIZATION, BEARER + accessToken);
        RequestObject requestObject = new RequestObject(requestUrl, Collections.singletonList(authHeader));
        String jsonString = httpAPIService.executeGetRequest(requestObject);
        JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
        if (jsonObject.has(VALUE)) {
            Gson gson = new GsonBuilder().create();
            usersList = gson.fromJson(jsonObject.getAsJsonArray(VALUE), new TypeToken<List<AzureUser>>() {
            }.getType());
        }
        return usersList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AzureGroup> getUserGroups(String userId, String accessToken) throws URISyntaxException, IOException {
        List<AzureGroup> userGroups = new ArrayList<>();
        String requestUrl = GRAPH_API_BASE_URL + USERS_ENDPOINT + SLASH + userId + MEMBERS_ENDPOINT;
        Header authHeader = new BasicHeader(AUTHORIZATION, BEARER + accessToken);
        RequestObject requestObject = new RequestObject(requestUrl, Collections.singletonList(authHeader));
        String jsonString = httpAPIService.executeGetRequest(requestObject);
        JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
        if (jsonObject.has(VALUE)) {
            Gson gson = new GsonBuilder().serializeNulls().create();
            userGroups = gson.fromJson(jsonObject.getAsJsonArray(VALUE), new TypeToken<List<AzureGroup>>() {
            }.getType());
        }
        return userGroups.stream().filter(t -> t.getDisplayName() != null).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JsonObject acquireAccessTokenFromRefreshToken(String refreshToken) throws IOException {
        JsonObject accessToken;
        List<NameValuePair> params = new ArrayList<>(6);
        params.add(new BasicNameValuePair(GRANT_TYPE, REFRESH_TOKEN));
        params.add(new BasicNameValuePair(CLIENT_ID, config.ehiAuthClientId()));
        params.add(new BasicNameValuePair(CLIENT_SECRET, config.ehiAuthClientSecret()));
        params.add(new BasicNameValuePair(REFRESH_TOKEN, refreshToken));
        RequestBody requestBody = new RequestBody(params);
        RequestObject requestObject = new RequestObject(requestBody, config.ehiAuthAccessTokenRequestURI(), null);
        String responseFromAzure = httpAPIService.executePostRequest(requestObject);
        accessToken = new JsonParser().parse(responseFromAzure).getAsJsonObject();
        return accessToken;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JsonObject acquireAccessTokenForAuthCode(final String authCode) throws IOException {
        JsonObject accessToken = new JsonObject();
        List<NameValuePair> params = new ArrayList<>(6);
        params.add(new BasicNameValuePair(CODE, authCode));
        params.add(new BasicNameValuePair(GRANT_TYPE, AUTHORIZATION_CODE));
        params.add(new BasicNameValuePair(REDIRECT_URI, config.ehiAuthRedirectURI()));
        params.add(new BasicNameValuePair(CLIENT_ID, config.ehiAuthClientId()));
        params.add(new BasicNameValuePair(CODE_VERIFIER, config.ehiAuthCodeVerifier()));
        params.add(new BasicNameValuePair(CLIENT_SECRET, config.ehiAuthClientSecret()));
        RequestBody requestBody = new RequestBody(params);
        RequestObject requestObject = new RequestObject(requestBody, config.ehiAuthAccessTokenRequestURI(), null);
        String responseFromAzure = httpAPIService.executePostRequest(requestObject);
        JsonObject jsonObject = new JsonParser().parse(responseFromAzure).getAsJsonObject();
        if (!accessToken.isJsonNull()) {
            accessToken.addProperty(ACCESS_TOKEN, jsonObject.get(ACCESS_TOKEN).getAsString());
            accessToken.addProperty(REFRESH_TOKEN, jsonObject.get(REFRESH_TOKEN).getAsString());
        }
        return accessToken;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String acquireAccessTokenForGraphAPI() throws IOException {
        List<NameValuePair> bodyParams = new ArrayList<>(4);
        bodyParams.add(new BasicNameValuePair(SCOPE, config.scope()));
        bodyParams.add(new BasicNameValuePair(CLIENT_ID, config.clientId()));
        bodyParams.add(new BasicNameValuePair(CLIENT_SECRET, config.clientSecret()));
        bodyParams.add(new BasicNameValuePair(GRANT_TYPE, config.grantType()));
        RequestBody requestBody = new RequestBody(bodyParams);
        String requestURL = MICROSOFT_LOGIN_URL + SLASH + config.tenantId() + TOKEN_ENDPOINT;
        RequestObject requestObject = new RequestObject(requestBody, requestURL, null);
        String jsonResponse = httpAPIService.executePostRequest(requestObject);
        JsonObject jsonObject = new JsonParser().parse(jsonResponse).getAsJsonObject();
        return jsonObject.has(ACCESS_TOKEN) ? jsonObject.get(ACCESS_TOKEN).getAsString() : StringUtils.EMPTY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DecodedJWT verifyToken(final String accessToken) throws MalformedURLException, JwkException {
        DecodedJWT jwt = JWT.decode(accessToken);
        logger.debug("verifyToken(...) [Debug] Subject from decoded jwt token: {}", jwt.getSubject());
        URL url = new URL(config.ehiAuthKeysURI());
        JwkProvider provider = new UrlJwkProvider(url);
        Jwk jwk = provider.get(jwt.getKeyId());
        Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
        algorithm.verify(jwt);
        return jwt;
    }
}
