package com.ehi.marketplace.core.services.impl;

import com.ehi.marketplace.core.beans.api.RequestBody;
import com.ehi.marketplace.core.beans.api.RequestObject;
import com.ehi.marketplace.core.services.HttpAPIService;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

import static java.nio.charset.StandardCharsets.UTF_8;

@Component(service = HttpAPIService.class, immediate = true)
public class HttpAPIServiceImpl implements HttpAPIService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpAPIServiceImpl.class);
    private static final Integer CONNECTION_TIMEOUT = 20000;
    private static final Integer SOCKET_TIMEOUT = 20000;
    
    @Reference
    private HttpClientBuilderFactory httpClientBuilderFactory;

    /**
     * {@inheritDoc}
     */
    @Override
    public String executeGetRequest(final RequestObject request) throws URISyntaxException, IOException {
        String url = request.getRequestUrl();
        final HttpClientBuilder builder = httpClientBuilderFactory.newBuilder();
        final RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECTION_TIMEOUT)
                .setSocketTimeout(SOCKET_TIMEOUT)
                .build();
        builder.setDefaultRequestConfig(requestConfig);
        final HttpClient httpClient = builder.build();
        URI uri;
        if (request.getParametersList() != null && !request.getParametersList().isEmpty()) {
            uri = new URIBuilder(url).addParameters(request.getParametersList()).build();
        } else {
            uri = new URIBuilder(url).build();
        }
        final HttpGet httpGetRequest = new HttpGet(uri);
        httpGetRequest.setHeaders(request.getHeaderList().toArray(new Header[0]));
        final HttpResponse resp = httpClient.execute(httpGetRequest);
        return getHttpResponse(resp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String executePostRequest(final RequestObject request) throws IOException {
        final RequestBody reqBody = request.getRequestBody();
        final HttpClientBuilder builder = httpClientBuilderFactory.newBuilder();
        final RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECTION_TIMEOUT)
                .setSocketTimeout(SOCKET_TIMEOUT)
                .build();
        builder.setDefaultRequestConfig(requestConfig);
        final HttpClient httpClient = builder.build();
        final HttpPost httpPostRequest = new HttpPost(request.getRequestUrl());
        if (reqBody != null) {
            if (reqBody.getMultiPartEntity() != null) {
                httpPostRequest.setEntity(reqBody.getMultiPartEntity().build());
            } else if (reqBody.getJsonEntity() != null) {
                httpPostRequest.setEntity(reqBody.getJsonEntity());
            } else if (reqBody.getBodyParams() != null && !reqBody.getBodyParams().isEmpty()) {
                httpPostRequest.setEntity(new UrlEncodedFormEntity(reqBody.getBodyParams(), UTF_8));
            }
        }
        if (request.getHeaderList() != null && !request.getHeaderList().isEmpty()) {
            httpPostRequest.setHeaders(request.getHeaderList().toArray(new Header[0]));
        }
        final HttpResponse resp = httpClient.execute(httpPostRequest);
        return getHttpResponse(resp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String executeDeleteRequest(final RequestObject request) throws URISyntaxException, IOException {
        String url = request.getRequestUrl();
        final HttpClientBuilder builder = httpClientBuilderFactory.newBuilder();
        final RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECTION_TIMEOUT)
                .setSocketTimeout(SOCKET_TIMEOUT)
                .build();
        builder.setDefaultRequestConfig(requestConfig);
        final HttpClient httpClient = builder.build();
        URI uri = new URIBuilder(url).build();
        final HttpDelete httpDeleteRequest = new HttpDelete(uri);
        httpDeleteRequest.setHeaders(request.getHeaderList().toArray(new Header[0]));
        final HttpResponse resp = httpClient.execute(httpDeleteRequest);
        return getHttpResponse(resp);
    }

    /**
     * Method getHttpResponse(...) method to Handle the Http Response.
     *
     * @param response HttpResponse
     * @return {@link String}
     * @throws IOException
     */
    private static String getHttpResponse(final HttpResponse response) throws IOException {
        int status = response.getStatusLine().getStatusCode();
        LOGGER.info("HTTP Status code : {}", status);
        if (status >= 200 && status <= 400) {
            HttpEntity entity = response.getEntity();
            if (Objects.isNull(entity)) {
                return null;
            } else {
                return EntityUtils.toString(entity);
            }
        } else {
            return null;
        }
    }
}
