package com.ehi.marketplace.core.services.impl;

import com.day.cq.dam.api.Asset;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.ehi.marketplace.core.services.JcrSearchService;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * {@inheritDoc}
 */
@Component(service = JcrSearchService.class, immediate = true)
public class JcrSearchServiceImpl implements JcrSearchService {

    private static final Logger log = LoggerFactory.getLogger(JcrSearchServiceImpl.class);

    @Reference
    private QueryBuilder queryBuilder;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Resource> getResources(Map<String, String> predicates, Session session) {
        Query query = queryBuilder.createQuery(PredicateGroup.create(predicates), session);
        return executeQuery(query);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Page> getPages(Map<String, String> predicates, Session session) {
        List<Resource> resources = getResources(predicates, session);
        return convertResources(resources, Page.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Asset> getAssets(Map<String, String> predicates, Session session) {
        List<Resource> resources = getResources(predicates, session);
        return convertResources(resources, Asset.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getTotalCount(Map<String, String> predicates, Session session) {
        Query query = queryBuilder.createQuery(PredicateGroup.create(predicates), session);
        SearchResult searchResult = query.getResult();
        long totalMatches = 0;
        if (searchResult != null) {
            totalMatches = searchResult.getTotalMatches();
        }
        return Math.toIntExact(totalMatches);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPathExist(final String filePath, final ResourceResolver resourceResolver) {
        boolean result = false;
        if (StringUtils.isNotBlank(filePath)) {
            result = checkFilePath(filePath, resourceResolver, result);
        } else {
            log.info("JcrSearchService : FilePath is empty");
        }
        return result;
    }

    private static List<Resource> executeQuery(Query query) {
        List<Resource> results = new ArrayList<>();
        SearchResult searchResult = query.getResult();
        Iterator<Resource> resources = searchResult.getResources();
        resources.forEachRemaining(results::add);
        return results;
    }

    private static <T> List<T> convertResources(List<Resource> resources, Class<T> classType) {
        List<T> results = new ArrayList<>();
        resources.forEach(resource -> {
            T element = resource.adaptTo(classType);
            if (element != null) {
                results.add(element);
            }
        });
        return results;
    }

    private boolean checkFilePath(String filePath, ResourceResolver resourceResolver, boolean result) {
        if (resourceResolver != null) {
            Resource resource = resourceResolver.getResource(filePath);
            if (resource != null) {
                result = true;
            }
        }
        return result;
    }
}
