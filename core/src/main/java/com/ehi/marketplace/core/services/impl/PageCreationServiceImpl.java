package com.ehi.marketplace.core.services.impl;

import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.day.cq.wcm.msm.api.LiveRelationshipManager;
import com.day.cq.wcm.msm.api.RolloutManager;
import com.day.cq.wcm.msm.api.RolloutManager.RolloutParams;
import com.ehi.marketplace.core.services.PageCreationService;

@Component(service = PageCreationService.class, immediate = true)
public class PageCreationServiceImpl implements PageCreationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PageCreationServiceImpl.class);

	@Reference
	private RolloutManager rolloutManager;

	@Reference
	LiveRelationshipManager liveRelManager;

	/**
	 * {@inheritDoc}
	 * @throws WCMException 
	 */
	@Override
	public Page createPage(String pagePath, String pageName, String templatePath, String jcrName, ResourceResolver resolver) throws IllegalArgumentException, WCMException, javax.jcr.PathNotFoundException{
		Page pageCreated = null;
		PageManager pageManager = resolver.adaptTo(PageManager.class);
		pageCreated = pageManager.create(pagePath, jcrName, templatePath, pageName);
		return pageCreated;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page checkPageExists(String pagePath, ResourceResolver resolver) {
		PageManager pageManager = resolver.adaptTo(PageManager.class);
		Page apiSpecPage = pageManager.getPage(pagePath);
		return apiSpecPage;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void rolloutPage(Page targetPage,ResourceResolver resolver) throws WCMException,NullPointerException {
		RolloutParams params = new RolloutParams();
		params.isDeep = false;
		params.master = targetPage;
		params.reset = false;
		params.trigger = RolloutManager.Trigger.ROLLOUT;
		params.delete = false;
		params.targets = null;
		rolloutManager.rollout(params);
	}
}

