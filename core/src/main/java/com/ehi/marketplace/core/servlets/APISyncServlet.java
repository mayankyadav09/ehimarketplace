package com.ehi.marketplace.core.servlets;

import com.day.cq.wcm.api.Page;
import com.ehi.marketplace.core.constants.EHIConstants;
import com.ehi.marketplace.core.services.APISyncService;
import com.ehi.marketplace.core.utils.EHIUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.event.jobs.JobManager;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component(service = {Servlet.class}, property = {
        ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_GET,
        ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/ehi/api/sync"
})
public class APISyncServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(APISyncServlet.class);

    private static final String JOB_TOPIC = "api/spec/job";

    @Reference
    private transient JobManager jobManager;

    @Reference
    private transient SlingSettingsService slingSettingsService;

    @Reference
    private transient ResourceResolverFactory resourceResolverFactory;

    @Reference
    private transient APISyncService apiSyncService;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        Set<String> runModes = slingSettingsService.getRunModes();
        if (runModes.contains("author")) {
            try (ResourceResolver resolver = EHIUtils
                    .getResourceResolver(resourceResolverFactory, EHIConstants.EHI_WRITE_DATA_SUB_SERVICE);) {
                Session session = resolver.adaptTo(Session.class);
                List<Page> apiSpecPages = apiSyncService.getAPISpecPages(session);
                for (Page apiSpecPage : apiSpecPages) {
                    Map<String, Object> jobProperties = new HashMap<>();
                    jobProperties.put("pagePath", apiSpecPage.getPath());
                    jobProperties.put("apiSpecId", apiSpecPage.getProperties().get(EHIConstants.API_SPEC_ID, ""));
                    jobManager.addJob(JOB_TOPIC, jobProperties);
                }
            } catch (Exception e) {
                log.error("Exception in :: doGet() :", e);
            }
            response.getWriter().println("Sling Job Assigned!");
            response.flushBuffer();
        }
    }
}
