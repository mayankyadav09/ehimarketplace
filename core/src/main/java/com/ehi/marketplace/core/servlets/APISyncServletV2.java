package com.ehi.marketplace.core.servlets;

import java.io.IOException;
import java.util.Set;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.ResourceNotFoundException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.settings.SlingSettingsService;
import org.joda.time.Instant;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMException;
import com.ehi.marketplace.core.constants.EHIConstants;
import com.ehi.marketplace.core.services.APISyncService;
import com.ehi.marketplace.core.services.PageCreationService;
import com.ehi.marketplace.core.utils.EHIUtils;

import io.swagger.parser.OpenAPIParser;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.parser.core.models.SwaggerParseResult;

@Component(service = {Servlet.class}, property = {
		ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_POST,
		ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/ehi/api/syncv2"
})
public class APISyncServletV2 extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(APISyncServletV2.class);

	@Reference
	private transient SlingSettingsService slingSettingsService;

	@Reference
	private transient ResourceResolverFactory resourceResolverFactory;

	@Reference
	private transient APISyncService apiSyncService;

	@Reference
	private PageCreationService pageCreationService;

	@Override
	protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		String responseMessage = EHIConstants.API_SPEC_NOT_UPDATED;
		String uniqueyKey = "";
		JSONObject jsonErrorObj = new JSONObject();
		Set<String> runModes = this.slingSettingsService.getRunModes();
		if (runModes.contains(EHIConstants.AUTHOR)) {
			try (ResourceResolver resolver = EHIUtils
					.getResourceResolver(resourceResolverFactory, EHIConstants.EHI_WRITE_DATA_SUB_SERVICE);) {
				String apiData = request.getParameter(EHIConstants.DATA);
				String version = request.getParameter(EHIConstants.VERSION);
				jsonErrorObj.put(EHIConstants.DATA, apiData);
				jsonErrorObj.put(EHIConstants.VERSION, version);
				uniqueyKey = EHIConstants.ERROR + Instant.now().toString();
				if(apiData != null && !apiData.isEmpty()) {
					SwaggerParseResult swaggerResult = new OpenAPIParser().readContents(apiData, null, null);
					OpenAPI openAPI = swaggerResult.getOpenAPI();
					if (openAPI != null) {
						log.debug("APISyncServletV2 :: doPost :: API data :: {}", openAPI.getInfo().getVersion());
						String apiSpecTitle= openAPI.getInfo().getTitle();
						log.debug("APISyncServletV2 :: doPost :: apiSpecId :: {}", apiSpecTitle );
						String parentApiSpecPageUrl = request.getParameter(EHIConstants.REQUEST_PATH);
						jsonErrorObj.put(EHIConstants.REQUEST_PATH, parentApiSpecPageUrl);
						log.debug("APISyncServletV2  :: doPost :: Parent api spec url {}", parentApiSpecPageUrl);
						String apiSpecPageUrl = parentApiSpecPageUrl + "/"+ createPageName(apiSpecTitle,version);
						log.debug("APISyncServletV2  :: doPost :: Main page Url {}", apiSpecPageUrl);
						Page apiSpecPage = pageCreationService.checkPageExists(apiSpecPageUrl, resolver);
						if(apiSpecPage == null) {
							log.debug("APISyncServletV2  :: doPost :: Creating new page with Page Version as {}", version);
							apiSpecPage = pageCreationService.createPage(parentApiSpecPageUrl, apiSpecTitle, EHIConstants.TEMPLATE_PATH, createPageName(apiSpecTitle,version), resolver);
							resolver.commit();
						}
						apiSyncService.putPropertiesInAPISpecPage(apiSpecPage,resolver,openAPI, apiData);
						pageCreationService.rolloutPage(apiSpecPage,resolver);
						resolver.commit();  
						responseMessage = EHIConstants.API_SPEC_UPDATED;
					}
				}
			} catch (PersistenceException | NullPointerException | IllegalArgumentException | WCMException| ResourceNotFoundException | javax.jcr.PathNotFoundException e ) {
				log.error("Exception in :: doPost() APISyncServletV2:: {}", e);
				apiSyncService.storeErrorLogs(jsonErrorObj,uniqueyKey,e.toString());
			}  catch (JSONException e) {
				log.error("Exception in :: doPost() APISyncServletV2:: {}", e);
			} catch (LoginException e) {
				log.error("Exception in :: doPost() APISyncServletV2:: {}", e);
			}
			response.getWriter().println(responseMessage);
			response.flushBuffer();
		}
	}

	private String createPageName(String title, String version) {
		return title.toLowerCase().replace(" ", "-") + EHIConstants.ENDPOINTS + "-" + version.split("[.]")[0];
	}
}
