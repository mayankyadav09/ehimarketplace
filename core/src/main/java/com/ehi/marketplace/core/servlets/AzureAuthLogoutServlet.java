package com.ehi.marketplace.core.servlets;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import java.io.IOException;

import static com.ehi.marketplace.core.constants.EHIConstants.AUTH_TOKEN;
import static com.ehi.marketplace.core.constants.EHIConstants.HTML_EXTENSION;
import static com.ehi.marketplace.core.constants.EHIConstants.SLASH;
import static org.apache.sling.api.servlets.HttpConstants.METHOD_GET;

@Component(service = Servlet.class, immediate = true)
@SlingServletResourceTypes(resourceTypes = {"ehimarketplace/components/structure/pages/page"},
        selectors = "azure.logout", methods = {METHOD_GET})
public class AzureAuthLogoutServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(AzureAuthLogoutServlet.class);

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        Cookie authCookie = request.getCookie(AUTH_TOKEN);
        if (authCookie != null) {
            authCookie.setValue(StringUtils.EMPTY);
            authCookie.setPath(SLASH);
            authCookie.setMaxAge(0);
            response.addCookie(authCookie);
        }
        Resource resource = request.getResource();
        InheritanceValueMap inheritanceValueMap = new HierarchyNodeInheritanceValueMap(resource);
        String logoutUrl = inheritanceValueMap.getInherited("logoutURL", "");
        logger.info("doGet(...) [Info] Logout URL: {}", logoutUrl);
        response.sendRedirect(logoutUrl + HTML_EXTENSION);
    }
}
