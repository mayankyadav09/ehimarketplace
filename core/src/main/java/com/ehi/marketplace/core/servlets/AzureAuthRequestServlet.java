package com.ehi.marketplace.core.servlets;

import com.adobe.granite.crypto.CryptoException;
import com.adobe.granite.crypto.CryptoSupport;
import com.auth0.jwk.JwkException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ehi.marketplace.core.beans.AzureGroup;
import com.ehi.marketplace.core.services.GraphAPIService;
import com.ehi.marketplace.core.utils.EHIUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static com.ehi.marketplace.core.constants.EHIConstants.ACCESS_TOKEN;
import static com.ehi.marketplace.core.constants.EHIConstants.AUTH_TOKEN;
import static com.ehi.marketplace.core.constants.EHIConstants.AZURE_GROUPS;
import static com.ehi.marketplace.core.constants.EHIConstants.CODE;
import static com.ehi.marketplace.core.constants.EHIConstants.HTML_EXTENSION;
import static com.ehi.marketplace.core.constants.EHIConstants.REFRESH_TOKEN;
import static com.ehi.marketplace.core.constants.EHIConstants.STATE;

@Component(service = {Servlet.class}, property = {
        ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_GET,
        ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/ehi/auth/code/verifier"
})
public class AzureAuthRequestServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(AzureAuthRequestServlet.class);

    @Reference
    private transient GraphAPIService graphAPIService;

    @Reference
    private transient CryptoSupport cryptoSupport;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        JsonObject authObject = new JsonObject();
        String authCode = request.getParameter(CODE);
        String requestPath = request.getParameter(STATE);
        Gson gson = new Gson();
        try {
            JsonObject jsonToken = graphAPIService.acquireAccessTokenForAuthCode(authCode);
            DecodedJWT jwt = graphAPIService.verifyToken(jsonToken.get(ACCESS_TOKEN).getAsString());
            String uid = jwt.getSubject();
            String graphAPIAccessToken = graphAPIService.acquireAccessTokenForGraphAPI();
            List<AzureGroup> userGroups = graphAPIService.getUserGroups(uid, graphAPIAccessToken);
            JsonElement jsonElement = gson.toJsonTree(userGroups);
            authObject.addProperty(ACCESS_TOKEN, jsonToken.get(ACCESS_TOKEN).getAsString());
            authObject.addProperty(REFRESH_TOKEN, jsonToken.get(REFRESH_TOKEN).getAsString());
            authObject.add(AZURE_GROUPS, jsonElement);
            log.debug("doGet(...) [Debug] Access Token Exists: {}", authObject.has(ACCESS_TOKEN));
            log.debug("doGet(...) [Debug] Valid Token and Redirecting to the Requested Page: {}", requestPath);
            EHIUtils.setCookie(response, cryptoSupport.protect(authObject.toString()), AUTH_TOKEN, null, 3600);
            response.sendRedirect(requestPath + HTML_EXTENSION);
            response.flushBuffer();
        } catch (JwkException | CryptoException | URISyntaxException err) {
            log.error("doPost(...) JwkException | CryptoException | URISyntaxException: ", err);
        }
    }
}
