package com.ehi.marketplace.core.utils;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.drew.lang.annotations.NotNull;
import com.ehi.marketplace.core.beans.AzureGroup;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.Cookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.day.cq.tagging.TagConstants.PN_TAGS;
import static org.apache.jackrabbit.JcrConstants.JCR_CONTENT;

public class EHIUtils {

    private static final Logger logger = LoggerFactory.getLogger(EHIUtils.class);

    public static final String DOMAIN_NAME = "ehimarketplace.com";

    private EHIUtils() {
    }

    public static ResourceResolver getResourceResolver(final ResourceResolverFactory resourceResolverFactory,
                                                       final String subService) throws LoginException {
        ResourceResolver resourceResolver = null;
        if (null != resourceResolverFactory && null != subService) {
            final Map<String, Object> authInfo = new HashMap<>();
            authInfo.put(ResourceResolverFactory.SUBSERVICE, subService);
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(authInfo);
        }
        return resourceResolver;
    }

    /**
     * Method to verify the user along with group!
     *
     * @param resolver
     * @param requestPath
     * @param azureGroups
     * @return
     */
    public static boolean verifyUserGroup(final ResourceResolver resolver, final String requestPath,
                                          final List<AzureGroup> azureGroups) {
        boolean isValidUser = false;
        List<String> groupsList = getListOfGroupsForPage(requestPath, resolver);
        if (groupsList.isEmpty()) {
            logger.debug("verifyUserGroup(...) [Debug] Group not verified!");
        } else {
            for (String group : groupsList) {
                if (group != null && azureGroups.stream().anyMatch(t -> t.getDisplayName()
                        .equals(group))) {
                    isValidUser = true;
                }
            }
        }
        return isValidUser;
    }

    /**
     * Method to get a list of all the groups assigned for a cq:Page
     *
     * @param pagePath
     * @param resolver
     * @return
     */
    public static List<String> getListOfGroupsForPage(final String pagePath, final ResourceResolver resolver) {
        List<String> groups = new ArrayList<>();
        Resource pageResource = resolver.getResource(pagePath);
        if (pageResource != null) {
            Resource jcrResource = resolver.getResource(pageResource, JCR_CONTENT);
            InheritanceValueMap inheritanceValueMap = new HierarchyNodeInheritanceValueMap(jcrResource);
            String[] tags = inheritanceValueMap.getInherited(PN_TAGS, String[].class);
            List<String> cqTags = Arrays.asList(tags != null ? tags : new String[]{});
            logger.debug("getListOfGroupsForPage(...) [Debug] cq:Tags: {}", cqTags);
            TagManager tagManager = resolver.adaptTo(TagManager.class);
            if (tagManager != null && !cqTags.isEmpty()) {
                cqTags.forEach(t -> {
                    Tag tag = tagManager.resolve(t);
                    if (tag != null && tag.getParent().getName().equalsIgnoreCase("group")) {
                        groups.add(tag.getName());
                    }
                });
            }
        }
        logger.debug("getListOfGroupsForPage(...) [Debug] list of groups: {}", groups);
        return groups;
    }

    /**
     * Method to validate whether the token is expired or not!
     *
     * @param jwt
     * @return
     */
    public static boolean isTokenExpired(DecodedJWT jwt) {
        return jwt.getExpiresAt().before(Calendar.getInstance().getTime());
    }

    /**
     * Method to set the cookie with the given fields
     *
     * @param response   SlingHttpServletResponse
     * @param value      String Value
     * @param cookieName String Cookie Name
     * @param domainName Domain Name
     */
    public static void setCookie(@NotNull SlingHttpServletResponse response, String value, String cookieName, String domainName, int time) {
        Cookie cookie = new Cookie(cookieName, value);
        cookie.setPath("/");
        cookie.setSecure(true);
        cookie.setMaxAge(time);
        if (StringUtils.isNotBlank(domainName))
            cookie.setDomain(DOMAIN_NAME);
        response.addCookie(cookie);
    }

    /**
     * @param request    SlingHttpServletRequest request
     * @param cookieName Cookie Name
     * @return String
     */
    public static String getCookieValue(SlingHttpServletRequest request, String cookieName) {
        if (Objects.nonNull(request)) {
            Cookie cookie = request.getCookie(cookieName);
            if (Objects.nonNull(cookie)) {
                return cookie.getValue();
            }
        }
        return StringUtils.EMPTY;
    }
}
