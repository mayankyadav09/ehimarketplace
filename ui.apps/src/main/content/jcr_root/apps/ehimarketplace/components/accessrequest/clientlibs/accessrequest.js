(function($) {
	$(document).ready(function() {
		$(".sendRequest").click(function(event) {
			event.preventDefault();
			var fullName = $(".fullName").val();
			$.ajax({
				type: 'POST',
				url: '/bin/accessrequest.txt',
				data: {
					fullName: fullName
				},
				success: function(data) {
					console.log("success=" + data);
				},
				error: function(data) {
					console.log('fail');
				}
			});
		})
	})
})($);